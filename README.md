# Bootstrap Host
Bootstrap Host Instructions for:
- [MacOS](bootstrap_brew.md)
- [Debian / Ubuntu / Pop! OS](bootstrap_apt.md)
- [SUSE](bootstrap_zypper.md)
- [Arch](bootstrap_pacman.md)
