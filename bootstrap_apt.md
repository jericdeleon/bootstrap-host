# Bootstrap - apt

## Station

1. Bootstrap host
    1. Install Ansible
        - `sudo apt install ansible`
    2. Download [bootstrap-host](https://gitlab.com/jericdeleon/bootstrap-host)
    3. Verify `playbook` values
    4. Verify `group_vars/all.yml` values
    5. Install ansible plugins:
        - `ansible-galaxy collection install community.general`
    6. Install ansible requirements
        - `sudo pip install github3.py`
    7. Run playbook: `./bootstrap.sh`
2. If shell is not set, reboot
3. Edit Settings for personal preferences
    1. Change Caps Lock to Control
4. Install [dotfiles](https://gitlab.com/jericdeleon/dotfiles/-/blob/master/dot_install_dotfiles.md)
5. Install runtimes in `asdf` for `vim`
    - `nodejs`
        - `eslint`
        - `import-sort-cli`
        - `prettier`
        - `typescript`
        - `vls`
    - `ruby`
        - `rubocop`
        - `solargraph`
6. Install repos
    - [dockerfiles](https://gitlab.com/jericdeleon/dockerfiles)
    - [notes](https://gitlab.com/jericdeleon/notes)
    - [ledger](https://gitlab.com/jericdeleon/ledger)
    - [password-store](https://gitlab.com/jericdeleon/password-store)
7. Setup input-remapper for laptop keyboard, with the ff. changes:
    - Goal
        - Caps_Lock -> disable
        - Super_L -> Alt_L
        - Alt_L -> Ctrl_L
        - Alt_R -> Super_R
    - Enable autoload, then apply
9. Pair bluetooth devices
    - If not pairing, [try blueman / bredr](https://github.com/pop-os/pop/issues/810)
10. Update at will / on distro upgrade
    1. Rerun bootstrap instructions (should be idempotent)
    2. Update dotfiles (bootstrap will overwrite some files)
